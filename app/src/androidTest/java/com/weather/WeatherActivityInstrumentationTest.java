package com.weather;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import com.weather.features.weather.WeatherActivity;
import com.weather.network.ServerApi;
import com.weather.pojo.Current;
import com.weather.pojo.Day;
import com.weather.pojo.Forecast;
import com.weather.pojo.ForecastDay;
import com.weather.pojo.Location;
import com.weather.pojo.Weather;

import junit.framework.AssertionFailedError;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

@RunWith(MockitoJUnitRunner.class)
public class WeatherActivityInstrumentationTest {
    @Mock
    ServerApi serverApi;

    @Rule
    public ActivityTestRule<WeatherActivity> mWeatherActivityTestRule = new ActivityTestRule<>(WeatherActivity.class);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchForecastData() {
        Mockito.when(serverApi.getWeather(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(Observable.just(getWeather()));

        TestObserver<Weather> testObserver = serverApi.getWeather(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt()).test();
        testObserver.awaitTerminalEvent();
        testObserver
                .assertNoErrors()
                .assertValue(weather -> weather.getForecast().getForecastDayList().size() == 2)
                .assertValue(weather -> weather.getLocation().getName().equalsIgnoreCase("Bangalore"));
    }

    @Test
    public void testUi() {
        try {
            Espresso.onView(ViewMatchers.withId(R.id.loader)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        } catch (AssertionFailedError e) {
            // View not displayed
        }
    }

    @Test
    public void testRecyclerView() {
        try {
            Espresso.onView(ViewMatchers.withId(R.id.recycler_view_forecast)).perform(RecyclerViewActions.scrollToPosition(3));
        } catch (AssertionFailedError e) {
            // View not displayed
        }
    }

    private Weather getWeather() {
        Weather weather = new Weather();
        Current current = new Current();
        current.setTemperaturInCelsius(20f);
        weather.setCurrent(current);

        Location location = new Location();
        location.setName("Bangalore");
        location.setEpochTime(1530180477L);
        weather.setLocation(location);

        Forecast forecast = new Forecast();
        ArrayList<ForecastDay> forecastList = new ArrayList<>(2);

        ForecastDay forecastDay1 = new ForecastDay();
        Day day1 = new Day();
        day1.setTemperaturInCelsius(29f);
        forecastDay1.setDay(day1);
        forecastList.add(forecastDay1);

        ForecastDay forecastDay2 = new ForecastDay();
        Day day2 = new Day();
        day2.setTemperaturInCelsius(23.4f);
        forecastDay2.setDay(day2);
        forecastList.add(forecastDay2);

        forecast.setForecastDayList(forecastList);
        weather.setForecast(forecast);
        return weather;
    }

}
