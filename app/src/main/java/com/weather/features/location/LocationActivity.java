package com.weather.features.location;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.weather.R;

public abstract class LocationActivity extends AppCompatActivity implements LocationNavigator {
    private final int PERMISSIONS_REQUEST_CODE = 1001;
    private final int REQUEST_CHECK_SETTINGS_CODE = 1002;
    protected LocationViewModel mLocationViewModel;

    public abstract void onLocationFetchFailed();

    public abstract void onLocationFetchSuccess(Location location);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocationViewModel = new LocationViewModel(this.getApplication());
        mLocationViewModel.setNavigator(this);
        mLocationViewModel.fetchLocation();
    }

    public void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (shouldProvideRationale) {
            handleError(getString(R.string.permission_rationale));
        } else {
            startLocationPermissionRequest();
        }
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mLocationViewModel.checkPermissions()) {
                    mLocationViewModel.getLocation();
                }
            }
        }
    }

    public void handleError(final String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }

    public void showEnableLocationDialog() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationViewModel.getLocationRequest())
                .setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mLocationViewModel.getGoogleApiClient(), builder.build());

        result.setResultCallback(locationSettingsResult -> {
            final Status status = locationSettingsResult.getStatus();

            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    mLocationViewModel.getLocation();
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        status.startResolutionForResult(LocationActivity.this, REQUEST_CHECK_SETTINGS_CODE);
                    } catch (IntentSender.SendIntentException e) {
                        onLocationFetchFailed();
                    }

                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    onLocationFetchFailed();
                    break;
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_CODE:
                switch (resultCode) {
                    case Activity.RESULT_CANCELED: {
                        onLocationFetchFailed();
                        break;
                    }
                    case Activity.RESULT_OK: {
                        mLocationViewModel.getLocation();
                        break;
                    }
                }
                break;
        }
    }
}
