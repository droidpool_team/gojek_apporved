package com.weather.features.location;

import android.location.Location;

interface LocationNavigator {
    void requestPermissions();

    void showEnableLocationDialog();

    void onLocationFetchSuccess(Location result);
    void onLocationFetchFailed();
}
