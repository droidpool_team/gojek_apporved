package com.weather.features.weather;

import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

import com.weather.R;
import com.weather.databinding.ActivityWeatherBinding;
import com.weather.features.location.LocationActivity;
import com.weather.features.weather.forecast.ForecastAdapter;

public class WeatherActivity extends LocationActivity implements WeatherNavigator {
    private WeatherViewModel mWeatherViewModel;
    private ActivityWeatherBinding mBinding;
    private ForecastAdapter mAdapter = null;
    private RotateAnimation mRotateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_weather);

        mWeatherViewModel = new WeatherViewModel(this.getApplication());
        mBinding.setViewModel(mWeatherViewModel);
        mWeatherViewModel.setNavigator(this);

        initUi();
    }

    private void initUi() {
        mBinding.btRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLocationViewModel.getLocation();
            }
        });
        subscribeUi();
        slideToTop(mBinding.recyclerViewForecast);
    }

    private void slideToTop(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,500,0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }

    private void subscribeUi() {
        mWeatherViewModel.getForecastList().observe(this,
                forecastList -> {
                    if (mAdapter == null) {
                        mAdapter = new ForecastAdapter(forecastList);
                        mBinding.recyclerViewForecast.setAdapter(mAdapter);
                        mBinding.recyclerViewForecast.setLayoutManager(new LinearLayoutManager(this));
                        mBinding.recyclerViewForecast.addItemDecoration(new DividerItemDecoration(this, RecyclerView.VERTICAL));
                    }
                        mAdapter.setForecastList(forecastList);
                        mAdapter.notifyDataSetChanged();
                });
    }

    @Override
    public void onLocationFetchFailed() {
        mWeatherViewModel.setIsError(true);
    }

    @Override
    public void onLocationFetchSuccess(Location location) {
        mWeatherViewModel.getWeather(location);
    }
}
