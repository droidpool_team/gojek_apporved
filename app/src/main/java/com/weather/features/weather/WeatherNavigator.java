package com.weather.features.weather;

interface WeatherNavigator {
    void handleError(String message);
}
