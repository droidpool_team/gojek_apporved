package com.weather.features.weather;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.location.Location;
import android.support.annotation.NonNull;

import com.weather.R;
import com.weather.Utils;
import com.weather.network.ApiClient;
import com.weather.pojo.ForecastDay;
import com.weather.pojo.Weather;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class WeatherViewModel extends AndroidViewModel {
    private Application mApplication;
    private WeatherNavigator mWeatherNavigator;

    private ObservableBoolean isError = new ObservableBoolean(false);
    private ObservableBoolean isData = new ObservableBoolean(false);
    private ObservableBoolean isLoading = new ObservableBoolean(true);

    private ObservableField<String> mDayTemperature = new ObservableField<>();
    private ObservableField<String> mLocation = new ObservableField<>();

    private MutableLiveData<List<ForecastDay>> mForecastList = new MutableLiveData<>();

    public WeatherViewModel(@NonNull Application application) {
        super(application);
        this.mApplication = application;
    }

    public void setNavigator(WeatherNavigator mWeatherNavigator) {
        this.mWeatherNavigator = mWeatherNavigator;
    }

    public void getWeather(Location location) {
        if (location != null) {
            StringBuilder locationBuilder = new StringBuilder();
            locationBuilder.append(location.getLatitude());
            locationBuilder.append(",");
            locationBuilder.append(location.getLongitude());

                if (Utils.isInternetAvailable(mApplication.getApplicationContext())) {
                    setIsError(false);
                    setIsLoading(true);

                    CompositeDisposable compositeDisposable = new CompositeDisposable();
                    compositeDisposable.add(ApiClient.getApiClient().getWeather(mApplication.getResources().getString(R.string.apixu_api_key), locationBuilder.toString(),
                            mApplication.getResources().getInteger(R.integer.weather_of_no_of_days))
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    weather -> {
                                        loadData(weather);
                                    },
                                    throwable -> {
                                        setIsError(true);
                                        if(throwable != null && throwable.getMessage() != null) {
                                            mWeatherNavigator.handleError(throwable.getMessage());
                                        }
                                    }
                            )
                    );
                } else {
                    setIsError(true);
                    mWeatherNavigator.handleError(mApplication.getResources().getString(R.string.network_error));
                }
        }
    }

    public void loadData(Weather weather) {
        if (weather != null) {
            setDayTemperature(String.valueOf(weather.getCurrent().getTemperaturInCelsius()));
            setLocation(weather.getLocation().getName());
            setForecastList(weather.getForecast().getForecastDayList());
            setIsData(true);
        }
    }

    public ObservableBoolean getIsError() {
        return isError;
    }

    public void setIsError(boolean isError) {
        this.isError.set(isError);
        setIsLoading(false);
    }

    public ObservableBoolean getIsData() {
        return isData;
    }

    public void setIsData(boolean isData) {
        this.isData.set(isData);
        setIsLoading(false);
    }

    public ObservableBoolean getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading.set(isLoading);
    }

    public ObservableField<String> getDayTemperature() {
        return mDayTemperature;
    }

    public void setDayTemperature(String temperature) {
        this.mDayTemperature.set(temperature);
    }

    public ObservableField<String> getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        this.mLocation.set(location);
    }

    public MutableLiveData<List<ForecastDay>> getForecastList() {
        return mForecastList;
    }

    public void setForecastList(List<ForecastDay> foreCastList) {
        this.mForecastList.setValue(foreCastList);
    }
}