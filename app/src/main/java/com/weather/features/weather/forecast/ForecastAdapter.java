package com.weather.features.weather.forecast;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.weather.BR;
import com.weather.R;
import com.weather.databinding.LayoutForecastItemBinding;
import com.weather.pojo.ForecastDay;

import java.util.ArrayList;
import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private ArrayList mForecastList = null;

    public ForecastAdapter(List<ForecastDay> list) {
        mForecastList = (ArrayList) list;
    }

    public void setForecastList(List<ForecastDay> list) {
        mForecastList = (ArrayList) list;
    }

    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        LayoutForecastItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_forecast_item, parent, false);
        return new ForecastViewHolder(binding);
    }

    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        if (mForecastList != null) {
            ForecastDay forecastDay = (ForecastDay) mForecastList.get(position);
            if (forecastDay != null) {
                ForecastViewModel viewModel = new ForecastViewModel();
                viewModel.setDayOfWeek(forecastDay.getEpochTime());

                if (forecastDay.getDay() != null) {
                    viewModel.setTemperature(forecastDay.getDay().getTemperaturInCelsius());
                }
                holder.bind(viewModel);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mForecastList != null) {
            return mForecastList.size();
        }
        return 0;
    }

    public class ForecastViewHolder extends RecyclerView.ViewHolder {
        private LayoutForecastItemBinding binding;

        public ForecastViewHolder(LayoutForecastItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ForecastViewModel viewModel) {
            binding.setVariable(BR.viewModel, viewModel);
            binding.executePendingBindings();
        }
    }
}
