package com.weather.features.weather.forecast;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;

import com.weather.BR;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ForecastViewModel extends BaseObservable {

    ObservableField<String> mDayOfWeek = new ObservableField<>();
    ObservableField<String>  mTemperature = new ObservableField();

    @Bindable
    public ObservableField<String> getDayOfWeek() {
        return mDayOfWeek;
    }

    public void setDayOfWeek(long epochTime) {
        Date date = new Date(epochTime * 1000);
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        String dayOfWeek = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        this.mDayOfWeek.set(dayOfWeek);
        notifyPropertyChanged(BR.dayOfWeek);
    }

    @Bindable
    public ObservableField<String> getTemperature() {
        return mTemperature;
    }

    public void setTemperature(float temperature) {
        this.mTemperature.set(String.valueOf(temperature));
        notifyPropertyChanged(BR.temperature);
    }
}
