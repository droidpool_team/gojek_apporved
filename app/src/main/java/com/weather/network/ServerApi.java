package com.weather.network;

import com.weather.pojo.Weather;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServerApi {

    String BASE_URL = "http://api.apixu.com/v1/";
    String WEATHER = "forecast.json?";

    @GET(WEATHER)
    Observable<Weather> getWeather(@Query("key") String apiKey, @Query("q")String location, @Query("days")int no_of_days);

}
