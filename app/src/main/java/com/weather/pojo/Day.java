package com.weather.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Day {
    @SerializedName("avgtemp_c")
    @Expose
    float temperaturInCelsius;

    public float getTemperaturInCelsius() {
        return temperaturInCelsius;
    }

    public void setTemperaturInCelsius(float temperaturInCelsius) {
        this.temperaturInCelsius = temperaturInCelsius;
    }
}
